package ru.friday.evgeniy.web_plz;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {
    private String[] DocTitles;
    private DrawerLayout dLayout;
    private ListView dList;
    private ActionBarDrawerToggle dToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    //Получаем доступ
        DocTitles = getResources().getStringArray(R.array.doc_array);
        dLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        dList = (ListView)findViewById(R.id.left_drawer);
        //Создаём список элементов
        dList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, DocTitles));
        dList.setOnItemClickListener(new DrawerItemClickListener());
        dToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                dLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        dLayout.setDrawerListener(dToggle);
        if (savedInstanceState == null) {
            selectItem(0);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = new TextFragment();
        Bundle args = new Bundle();
        args.putInt("NUMBER", position);
        fragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        dList.setItemChecked(position, true);
        dLayout.closeDrawer(dList);
    }

    public static class TextFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_text, container, false);
            int i = getArguments().getInt("NUMBER");
            Resources r = this.getResources();
            InputStream input;
            switch(i)
            {
                case 0:
                    input = r.openRawResource(R.raw.php_1);
                    break;
                case 1:
                    input = r.openRawResource(R.raw.php_2);
                    break;
                case 2:
                    input = r.openRawResource(R.raw.php_3);
                    break;
                case 3:
                    input = r.openRawResource(R.raw.php_4);
                    break;
                case 4:
                    input = r.openRawResource(R.raw.php_5);
                    break;
                case 5:
                    input = r.openRawResource(R.raw.php_6);
                    break;
                case 6:
                    input = r.openRawResource(R.raw.php_7);
                    break;
                case 7:
                    input = r.openRawResource(R.raw.php_8);
                    break;
                default:
                    input = r.openRawResource(R.raw.php_1);
            }
            try
            {
                String temp = toString(input);
                input.close();
                WebView Web = (WebView)rootView.findViewById(R.id.doc_text);
                Web.loadData(temp, "text/html", "utf-8");
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return rootView;
        }

        public String toString(InputStream in) throws IOException
        {
            ByteArrayOutputStream temp = new ByteArrayOutputStream();
            int i = in.read();
            while(i != -1)
            {
                temp.write(i);
                i = in.read();
            }
            return temp.toString();
        }
    }
}
